import os
import time
import random
import subprocess
scripts = []

import sys

ROOT_DIR = "/fs/clip-psych/sathvik/lc_surprisal/"
script = os.path.join(ROOT_DIR, "resource-rational-surprisal/model/compute_surprisal/runModel_Simplified.py")

import glob
models = glob.glob(os.path.join(ROOT_DIR, "CODEBOOKS_MEMORY/char-lm-ud-stationary_12_SuperLong_WithAutoencoder_WithEx_Samples_Short_Combination_Subseq_VeryLong_WithSurp12_NormJudg_Short_Cond_Shift_NoComma_Bugfix_VN3Stims_3_W_GPT2M_S.py_*.model"))
random.shuffle(models)
print("loaded retention probs")
# limit = 1000
limit = 1000

# if len(sys.argv) > 1:
#   limit = int(sys.argv[1])

count = 0

# CONTEXT_LENGTH = sys.argv[1]
# DELETION_RATE = 0.95
OUTPUT_DIR = os.path.join(ROOT_DIR, "data", "sap_agr")

logfile = "logsByScript/char-lm-ud-stationary_12_SuperLong_WithAutoencoder_WithEx_Samples_Short_Combination_Subseq_VeryLong_WithSurp12_NormJudg_Short_Cond_Shift_NoComma_Bugfix_VN3Stims_3_W_GPT2M_S.py.tsv"
log_path = os.path.join(ROOT_DIR, "resource-rational-surprisal/model/compute_surprisal", logfile)

with open(log_path, "r") as inFile:
   model_logs = [[q.strip() for q in x.split("\t")] for x in inFile.read().strip().split("\n")]
   model_logs = {x[0] : x for x in model_logs}

print(len(models))
for model in models:
   ID = model[model.rfind("_")+1:model.rfind(".")]
   resultsPath = os.path.join(OUTPUT_DIR, f"{script}_{ID}_Model")
   if len(glob.glob(resultsPath))>0:
     if os.path.getsize(resultsPath) > 0 and time.time() - os.stat(resultsPath).st_mtime < 3600: # written to within the last hour
       print("WORKING ON THIS?", ID)
       continue
     if os.path.getsize(resultsPath) > 1000:
        print("EXISTS", ID, os.path.getsize(resultsPath))
        continue
     print("TODO", ID, os.path.getsize(resultsPath))
   #pdb.set_trace()
   if ID in model_logs:
      model_log = model_logs[ID]
      # args = dict([x.split("=") for x in model_log[1] ])
      # this is getting read as a string
      model_arglist = model_log[1].replace("Namespace(N", "N")[:-1].split(",")
      model_argdict = dict([x.split("=") for x in model_arglist])
      args = {key.strip(): model_argdict[key] for key in model_argdict.keys()}
      delta = float(args["deletion_rate"])
      lambda_ = float(args["predictability_weight"])
      
      if lambda_ != 1:
        print("FOR NOW DON'T CONSIDER")
        continue
   else:
       print("Cannot find this model:", ID)
       count += 1
       continue
   print("DOES NOT EXIST", ID, delta, lambda_)
   command = ["python3", script, "--load_from_joint="+ID]
   print(command)
   subprocess.call(command)
   count += 1
   if count >= limit:
     print("???????")
     break
