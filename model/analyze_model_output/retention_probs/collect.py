import glob
#files = sorted(glob.glob("/fs/clip-psych/sathvik/lc_surprisal/resource-rational-surprisal/model/compute_surprisal/logsByScript/char*GPT2M_*S.py_*"))
files = sorted(glob.glob("/fs/clip-psych/sathvik/lc_surprisal/*.out"))
with open(f"retention_probs.tsv", "w") as outFile:
 print("\t".join(["Word", "Distance", "RetentionProb","Script"]), file=outFile)
 for f in files:
   print(f)
   with open(f, "r") as inFile:
     for line in inFile:
         # args = dict([x.split("=") for x in line.replace("Namespace(", "").rstrip(")").split(", ")])
         if line.startswith("SCORES"): # and "POS=" in line
             line = line.split("\t")
             word = line[0].replace("SCORES", "").strip()
             probs = line[1].strip().split(" ")
             probs = [float(x) for x in probs]
             suffix = f[:f.rfind("_")-3]
             suffix = suffix[-7:]
             for i in range(len(probs)-1, -1, -1):
                 print("\t".join([str(q) for q in [word, len(probs)-i, probs[i], suffix]]), file=outFile)
     
