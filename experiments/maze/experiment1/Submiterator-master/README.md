# Results (Experiment 1)

* [analyze.R](main analysis: analyzing reading times)
* [analyze_Outliers.R](analysis using alternative, less aggressive, outlier removal)
* [analyze_ErrorsStat.R](analyze errors), [output/analyze_ErrorsStat.R.txt](results)
* [annotateWordFreq.py](helper that adds word frequency information)
* [processTrialsByWord.py](helper that processes `trials.tsv` into a format read by the analysis scripts)
* [trials.tsv](full RT data), [trials_byWord.tsv](grouped by words)
